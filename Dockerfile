FROM python:3.6.15-slim-buster AS build

COPY . .

RUN pip install -r requirements.txt

EXPOSE 5000

FROM build



CMD ["python", "main.py"]