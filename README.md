
# Take home test

## Description
Take home test, is a demo application to collect data from weather source and make it available. It consists in a  RedisDB to cache data and generate indepotente enviroment, allowing deduplication enviroment for each weather. In terms of big data queries. We use a colunar database called Questdb where is capable to return billions of aggregations in a second.

## Quick Start

*PRE-REQUISITS*:
- Docker - Version 18.09 +(Higher)
- Docker Compose - Version 1.18.0 +(Higher)
*** 

### Docker Instalation

Docker docs: https://docs.docker.com/install/

Docker tutorial: 
- Linux Ubuntu - https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04


### Download code
Download the code on: https://gitlab.com/Menniti/take-home

Using git:

```
git clone https://gitlab.com/Menniti/take-home.git

```

## LOCALHOST NODE

### Run - Docker

```
cd take-home
docker-compose -f docker-compose.yml up -d --build
```

Docker Logs:

``` 
docker logs -f "container_hash_id"
```

### Stop Run - Docker
```
docker-compose -f docker-compose.yml down
```

### API URL`s
BASE: 
- localhost:5000/

METHODS: GET

PATH:
- /upinsert_weather_redis?countries=[[33.44,-94.04],[36.20,138.25]]&days_ago=5
- /upinsert_weather_redis - will get mock data

##### *update redis_to_questdb set a delay of 3 minutes to execute again. It avoids duplication in questdb. All functions above checks the delay before update.
- /update_redis_to_questdb
- /materialize_weather_by_day
- /materialize_weather_by_month


