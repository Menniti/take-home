import sys
import os

path = os.getcwd()+'/src'

for file in os.listdir(path):
    path_file = path+"/"+file
    sys.path.append(path_file)

from src.utils import *
from src.weather import *
from src.questdb import *

from flask import Flask, json, request

countries_mock = """[[33.44,-94.04],
    [36.20,138.25],
    [35.00,103.00],
    [55.75,37.61],
    [-33.45,-53.23]]
"""
def get_response(status,message):
    response = {
        "status":"",
        "message":""
    }
    response['status'] = status
    response['message'] = message
    return response


api = Flask(__name__)

@api.route('/materialize_weather_by_day', methods=['GET'])
def service_materialize_weather_by_day():
    
    if(get_compare_now_vs_last_date()!=True):
        return json.dumps(get_response("FAIL", "INFO: Please wait few more minutes to request it again"))
    try:
        materialize_weather_by_day()
    except Exception as error:
        logging("ERROR",("Error to materialize_weather_by_day - {0}").format(error))
        return json.dumps(get_response("FAIL", "ERROR: Not possible to materialize weather by day"))
    return json.dumps(get_response("OK", "INFO: materialize weather by day sucessfully"))

@api.route('/materialize_weather_by_month', methods=['GET'])
def service_materialize_weather_by_month():
    if(get_compare_now_vs_last_date()!=True):
        return json.dumps(get_response("FAIL", "INFO: Please wait few more minutes to request it again"))
    try:
        materialize_weather_by_month()
    except Exception as error:
        logging("ERROR",("Error to materialize_weather_by_month - {0}").format(error))
        return json.dumps(get_response("FAIL", "ERROR: Not possible to materialize weather by month"))
    return json.dumps(get_response("OK", "INFO: materialize weather by month sucessfully"))

@api.route('/upinsert_weather_redis', methods=['GET'])
def service_upinsert_weather_redis():
    
    args = request.args
    countries = args.get("countries")
    if countries == None:
        countries = countries_mock
    days_ago = args.get("days_ago")
    if days_ago == None:
        days_ago = 5
    countries = eval(countries)
    days_ago = int(days_ago)
    try:
        
        upinsert_weather_redis(countries, days_ago)
    except Exception as error:
        logging("ERROR",("Error to upsert_weather_redis - {0}").format(error))
        return json.dumps(get_response("FAIL", "ERROR: Not possible to update weathers in redis"))
    return json.dumps(get_response("OK", "INFO: Update weathers in redis sucessfully"))


@api.route('/update_redis_to_questdb', methods=['GET'])
def service_update_redis_to_questdb():
    
    args = request.args
    countries = args.get("countries")
    if countries == None:
        countries = countries_mock
        
    if(compare_last_update_redis_to_quest()!=True):
        return json.dumps(get_response("FAIL", "INFO: Please wait few more minutes to request it again"))
    try:
        update_redis_to_questdb(countries)
    except Exception as error:
        logging("ERROR","Not possible to Stream to questdb via influx - {0}".format(error))
        return json.dumps(get_response("FAIL", "ERROR: Not possible to update redis to questdb"))
    return json.dumps(get_response("OK", "INFO: Update redis to questdb sucessfully"))

if __name__ == '__main__':
    api.run(host="0.0.0.0", port=5000, debug=False) 