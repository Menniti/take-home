import psycopg2
import socket
from src.utils import *

def get_conn_questdb():
    try:
        connection = psycopg2.connect(user="admin",
                                      password="quest",
                                      host="questdb",
                                      port="8812",
                                      database="qdb")

        return connection

    except (Exception, psycopg2.Error) as error:
        logging("ERROR",("Error to connect in to Questdb using postgresql wire - {0}").format(error))
        return 
    
def db_execute(query):
    conn = get_conn_questdb()
    cur = conn.cursor()
    cur.execute(query)
    return cur

def db_select_all(query):
    cur = db_execute(query)
    rows = cur.fetchall()
    return rows




# For UDP, change socket.SOCK_STREAM to socket.SOCK_DGRAM

def stream_influx(array_data):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    HOST = 'questdb'
    PORT = 9009 
    try:
      sock.connect((HOST, PORT))

    except socket.error as e:
      
        logging("ERROR",("Error to connect in Questdb Socket - {0}").format(e))
        return

    for row in array_data:
        sock.sendall((row).encode())
