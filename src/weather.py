#weather.py
from flask import json

from src.redisdb import *
from src.questdb import *
import pandas as pd
from src.utils import *
import requests

countries_mock = [
    [33.44,-94.04],
    [36.20,138.25],
    [35.00,103.00],
    [55.75,37.61],
    [-33.45,-53.23]
]

config = {
    "appid":"12a6e86ea08bc4c8dffc35a4676a3253",
    "redis_host":"localhost",
    "redis_port":6379
}

def upinsert_weather_redis(countries, number_of_days):

    days_ago_timestamp = get_timestamp_from_datetime(get_n_days_ago_from_now(number_of_days))
    pipe = get_redis_pipeline()
    for country in countries:
        url = ('https://api.openweathermap.org/data/2.5/onecall/timemachine?lat={lat}&lon={lon}&dt={dt}&appid={appid}').format(lat=country[0],lon=country[1],dt=days_ago_timestamp,appid=config['appid'])
        try:
            logging("INFO",("Get request into url {0} and country location {1}").format(url,country))
            r = requests.get(url)
        except Exception as error:
            print(error)
            logging("ERROR",("Not possible to do request into country {0} and url {1}").format(country, url))
            continue
            
        weather = json.loads(r.text)
        
        for day in weather['hourly']:
            timestamp_location = day['dt']
            _, timestamp_utc, timestamp_utc_influx = format_timestamp_utc(timestamp_location, weather['timezone_offset'])
            location = "{latitude},{longitude}".format(latitude=weather['lat'],longitude=weather['lon']) 
            key = location+"_"+str(timestamp_utc)
            row_influx = "weather key=\"{key}\",timestamp_location={timestamp_location},location=\"{location}\",timezone='{timezone}',temperature={temp},latitude={latitude},longitude={longitude} {timestamp_utc}\n".format(timestamp_utc=timestamp_utc_influx, temp=day['temp'], timezone=weather['timezone'], latitude=weather['lat'], longitude=weather['lon'], location=location, timestamp_location=int(timestamp_location), key=key)
            json_row = {
                "key":key,
                "value":row_influx
            }
            pipe.set(key, json.dumps(json_row))
    pipe.execute()


def get_keys_weathers_questdb(today, days_ago):
    query = "SELECT key FROM weather WHERE timestamp BETWEEN '{start_date}' AND '{end_date}';".format(start_date=today,end_date=days_ago)
    rows = []
    try:
        logging("INFO","Query questdb -> {0}".format(query))
        rows = db_select_all(query)
        rows = str(rows).replace("(","")
        rows = rows.replace(",)","")
        rows = eval(rows)
        return rows
    except Exception as error:
        logging("ERROR",(str(error)))
        logging("INFO",("Returning empty array - {0}").format(query))
    return rows

def get_weathers_redis(countries):
    
    pipe = get_redis_pipeline()
    logging("INFO","Get keys from countries redis")
    for country in countries:
        match = str(country[0])+","+str(country[1])+'_*'
        pipe.keys(match)
        
    list_of_weathers = pipe.execute()
    list_of_keys_redis = []

    for weathers in list_of_weathers:
        for item_weathers in weathers:
            if item_weathers != None:
                w_str = str(item_weathers.decode())
                list_of_keys_redis.append(w_str)
    logging("INFO","Get objects using keys from redis")
    for key in list_of_keys_redis:
        pipe.get(key)
    list_weathers = pipe.execute()
    
    return list_weathers

def compare_redis_vs_questdb(list_weathers):
    
    today = datetime.now()
    days_ago = get_n_days_ago_from_now(5)

    rows = get_keys_weathers_questdb(today, days_ago)
    list_to_ingest_influx = []
    logging("INFO","Comparing redis vs questdb")
    for weather_json in list_weathers:
        weather_json = json.loads(weather_json.decode())
        if weather_json['key'] not in rows:
            weather_json['timestamp'] = weather_json["value"][-20:-1]
            w_key = weather_json['key']
            w_list = w_key.split("_")
            if int(w_list[1]) > get_timestamp_from_datetime(days_ago):
                list_to_ingest_influx.append(weather_json)
    logging("INFO","Return list of data to questdb add in influx format")
    return list_to_ingest_influx



def update_redis_to_questdb(countries):
    logging("INFO","Start redis to questdb")
    weathers = get_weathers_redis(countries)

    unordered_influx_lines = compare_redis_vs_questdb(weathers)
    
    sorted_influx_line = sorted(unordered_influx_lines, key=lambda d: int(d['timestamp']))
    
    weathers_influx = []
    
    for weather in sorted_influx_line:
        weathers_influx.append(weather['value'])
    logging("INFO","Stream to questdb influx - {0}".format(weathers_influx))
    stream_influx(weathers_influx)

def get_df_merge_weather_by_day():
    conn = get_conn_questdb()
    df = pd.read_sql("""

        WITH temperature_min AS (
            SELECT
            location AS min_location,
            MIN(temperature) AS min_temp,
            AVG(temperature) AS avg_temp,
            timestamp_floor('d', timestamp) AS min_day
            FROM weather 
            GROUP BY min_location, min_day
        ), temperature_max AS (
            SELECT
            location AS max_location,
            MAX(temperature) AS max_temp,
            timestamp_floor('d', timestamp) AS max_day
            FROM weather 
            GROUP BY max_location, max_day
        ), merge AS (
            SELECT 
                *,
                SUM(max_temp - min_temp) AS 'diff_temp'
            FROM temperature_min
            INNER JOIN temperature_max ON (temperature_max.max_day = temperature_min.min_day)
        )

        SELECT * FROM merge;

    """, conn)
    df2 = df.groupby(by="min_day").max("diff_temp")
    df2['max_day'] = df2.index.values
    
    df_merge = pd.merge(df,df2,how='inner',left_on=['max_day','diff_temp'],right_on=['max_day','diff_temp'])
    df_merge = df_merge.drop(['max_day', 'max_temp_x','diff_temp','min_temp_y','max_temp_y','avg_temp_y'], axis=1)
    return df_merge

def get_weather_by_day():
    weather_by_day = []
    try:
        conn = get_conn_questdb()
        df_weather_by_day = pd.read_sql("""
            SELECT day FROM weather_by_day;

        """, conn)
        weather_by_day = df_weather_by_day['day'].to_numpy()
    except Exception as error:
        logging("ERROR",("Error to select weather by day - {0}").format(error))
        logging("INFO",("Returning empty array"))
    return weather_by_day

def materialize_weather_by_day():
    df_merge = get_df_merge_weather_by_day()
    weather_by_day = get_weather_by_day()
    influx_lines = []
    for index, row in df_merge.iterrows():
        datetime = row['min_day'].strftime('%Y-%m-%d')
        if datetime not in weather_by_day:
            timestamp_influx = int(row['min_day'].timestamp()) * 1000000000
            row_influx = "weather_by_day day=\"{day}\",min_location=\"{min_location}\",max_location=\"{max_location}\",min_temperature={min_temperature},average_temperature={average_temperature} {timestamp_utc}\n".format(timestamp_utc=timestamp_influx, day=datetime, min_location=row['min_location'], max_location=row['max_location'], min_temperature=row['min_temp_x'], average_temperature=row['avg_temp_x'])
            influx_lines.append(row_influx)
    logging("INFO","Stream to questdb via unflux - {0}".format(influx_lines))
    stream_influx(influx_lines)

def get_df_agg_weather():
    conn = get_conn_questdb()
    df = ''
    try:
        df = pd.read_sql("""
            SELECT 
                location, 
                MAX(temperature) AS max_temperature,
                timestamp_floor('M', timestamp) AS month
            FROM weather GROUP BY month, location
        """, conn)
    except Exception as error:
        logging("ERROR",("Error to select weather aggregate - {0}").format(error))
        logging("INFO",("Returning empty array"))
    return df

def get_weather_by_month():
    conn = get_conn_questdb()
    weather_by_month = []
    df = ''
    try:
        df = pd.read_sql("""
            SELECT month FROM weather_by_month
        """, conn)
        weather_by_month = df['month'].to_numpy()
    except Exception as error:
        logging("ERROR",("Error to select weather_by_month - {0}").format(error))
        logging("INFO",("Returning empty array"))
        
    return weather_by_month

def materialize_weather_by_month():
    df_agg_weather = get_df_agg_weather()
    weather_by_month = get_weather_by_month()
    influx_lines = []
    for _, row in df_agg_weather.iterrows():
        datetime = row['month'].strftime('%Y-%m-%d')
        if datetime not in weather_by_month:
            timestamp_influx = int(row['month'].timestamp()) * 1000000000
            row_influx = "weather_by_month month=\"{month}\",location=\"{location}\",max_temperature={max_temperature} {timestamp_utc}\n".format(timestamp_utc=timestamp_influx, month=datetime, location=row['location'], max_temperature=row['max_temperature'])
            influx_lines.append(row_influx)
    logging("INFO","Stream to questdb via influx- {0}".format(influx_lines))
    stream_influx(influx_lines)