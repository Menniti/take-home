import redis
import socket


def get_redis_conn():
    conn = ''
    try:
        conn = redis.Redis(host='redis', port=6379, db=0, password="SUASENHA", socket_timeout=None, connection_pool=None, charset='utf-8', errors='strict', unix_socket_path=None)
    except socket.error as e:
      print("Got error: %s" % (e))
        
    return conn

def get_redis_pipeline():
    redis = get_redis_conn()
    pipeline = redis.pipeline()
    return pipeline

