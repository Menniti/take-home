from datetime import date, timedelta, datetime
import time
from src.redisdb import *
import json

##utils.py
def get_today():
    today = datetime.now()
    today = today.replace(minute=0)
    today = today.replace(second=0)
    return today

def get_n_days_ago_from_now(n=5):
    days_ago = datetime.now() - timedelta(days=n)
    return days_ago

def get_timestamp_from_datetime(datetime):
    timestamp = str(int(time.mktime(datetime.timetuple())))
    return int(timestamp) 

def format_timestamp_utc(timestamp_location, weather_timezone_offset):
    timestamp_utc = timestamp_location + weather_timezone_offset
    dt_object = datetime.fromtimestamp(timestamp_utc)
    dt_object = dt_object.replace(minute=0)
    dt_object = dt_object.replace(second=0)
    timestamp_utc_format = dt_object.strftime("%Y-%m-%d %H:%M:%S")
    timestamp_utc_influx = timestamp_utc * 1000000000
    return timestamp_utc_format, timestamp_utc, timestamp_utc_influx

def compare_last_update_redis_to_quest():
    
    now = datetime.now()
    now_str = str(now)
    key = "last_date"
    
    pipe = get_redis_pipeline()
    pipe.get(key)
    last_date = pipe.execute()

    json_row = {
            "key":key,
            "value":now_str
        }

    if  last_date[0]==None:
        pipe.set(key, json.dumps(json_row))
        pipe.execute()
        return True
    
    last_date = json.loads(last_date[0].decode())
    last_date = datetime.strptime(last_date['value'], "%Y-%m-%d %H:%M:%S.%f")
    elapsed = now - last_date
    elapsed_list = str(elapsed).split(":")
    logging("INFO","Last time update questdb get updated from redis - {0}".format(last_date))

    if int(elapsed_list[1]) > 2:
        pipe.set(key, json.dumps(json_row))
        pipe.execute()
        return True
    
    return False

def get_compare_now_vs_last_date():
    now = datetime.now()
    key = "last_date"
    pipe = get_redis_pipeline()
    pipe.get(key)
    last_date = pipe.execute()
    if last_date[0]!=None:
        last_date = json.loads(last_date[0].decode())
        last_date = datetime.strptime(last_date['value'], "%Y-%m-%d %H:%M:%S.%f")
        elapsed = now - last_date
        elapsed_list = str(elapsed).split(":")
        logging("INFO","Last time update questdb get updated from redis - {0}".format(last_date))
        if int(elapsed_list[1]) > 2:
            return True

def force_now_to_lastdate():
    now = datetime.now()
    now_str = str(now)
    key = "last_date"
    json_row = {
        "key":key,
        "value":now_str
    }
    pipe = get_redis_pipeline()
    pipe.set(key, json.dumps(json_row))
    pipe.execute()

def logging(status, message):
    print(('{0} - {1}:{2}').format(datetime.now(),status, message))
